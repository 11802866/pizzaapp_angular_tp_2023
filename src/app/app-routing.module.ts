import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListPizzaComponent} from "./pizza//list-pizza/list-pizza.component";
import {DetailsPizzaComponent} from "./pizza/details-pizza/details-pizza.component";
import { PizzaEditComponent } from './pizza/pizza-edit/pizza-edit.component';

const routes: Routes = [
  {path: 'pizzas', component: ListPizzaComponent },
  {path: 'pizza/edit/:id', component: PizzaEditComponent}, 
  {path: 'pizzas/:id', component: DetailsPizzaComponent },
  {path: '', redirectTo:'pizzas',  pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
