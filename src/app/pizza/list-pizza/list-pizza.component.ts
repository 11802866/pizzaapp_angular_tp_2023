import {Component, OnInit} from '@angular/core';
import {Pizza} from "../../Pizza";
import {LIST_PIZZAS} from "../../shared/list.pizza";
import {single} from "rxjs";
import {Router} from "@angular/router";
@Component({
  selector: 'app-list-pizza',
  templateUrl: './list-pizza.component.html',
  styleUrls: ['./list-pizza.component.scss']
})
export class ListPizzaComponent implements OnInit{

    PIZZAS: Pizza[] = [];

    constructor(private router:Router) {
    }

    ngOnInit() {
      this.PIZZAS=LIST_PIZZAS;
    }

    selectPizza(selectedPizza: Pizza) {
     //alert('vous avez sélectionné: ' + selectedPizza.name)
      const link: any = ['pizzas',selectedPizza.id];
      this.router.navigate(link);
    }
  protected readonly single = single;
}
