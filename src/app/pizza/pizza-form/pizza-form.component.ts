import {Component, Input, OnInit} from '@angular/core';
import {Pizza} from "../../Pizza";
import {ActivatedRoute, Router} from "@angular/router";
import {LIST_PIZZAS} from "../../shared/list.pizza";
import {PizzasService } from "../services-pizza.service";
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-pizza-form',
  templateUrl: './pizza-form.component.html',
  styleUrls: ['./pizza-form.component.scss']
})
export class PizzaFormComponent implements OnInit {

  constructor(private router: Router, private pizzaService: PizzasService ) { }

  ingredients : string[] | undefined;
  @Input() pizza : Pizza | undefined;

  ngOnInit(): void {
    this.ingredients = this.pizzaService.getPizzaIngredients();
  }
  hasIngredient(type: string): boolean{
    const index = this.pizza?.compositions?.indexOf(type);
    return (index !== -1);
  }
  selectIngredient($event: any, ingredient: string) : void{
    const checked= $event.target.checked;
    if (checked){
      this.pizza?.compositions?.push(ingredient);
    } else {
      const index = this.pizza?.compositions?.indexOf(ingredient)
      if (index && index > -1){
        this.pizza?.compositions?.splice(index, 1);
      }
    }
  }

  iscompositionValid(ingredient:string):boolean{

    if (this.pizza?.compositions?.length === 1 && this.hasIngredient(ingredient)){

      return false;
    }
    if (this.pizza?.compositions && this.pizza?.compositions.length>=3 && !this.hasIngredient(ingredient)){
      return false;
    }

    return true;
  }

  onSubmit(): void{
    console.log('Submit Form !')
    const lien = ['/pizzas', this.pizza?.id];
    this.router.navigate(lien);
  }

}

