import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pizza } from 'src/app/Pizza';
import { PizzasService } from '../services-pizza.service';

@Component({
  selector: 'app-pizza-edit',
  templateUrl: './pizza-edit.component.html',
  styleUrls: ['./pizza-edit.component.scss']
})
export class PizzaEditComponent {

  singlePizza: Pizza | undefined;

  constructor(private route: ActivatedRoute, private pizzaService: PizzasService) {
  }


  ngOnInit(): void {
    const retrieveIdFromUrl = +this.route.snapshot.params['id'];
    this.singlePizza = this.pizzaService.getSinglePizza(retrieveIdFromUrl);
    console.log("Vous avez selectionné : ", this.singlePizza);
  }

}
