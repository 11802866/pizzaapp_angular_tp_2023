import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Pizza } from "../../Pizza";
import { LIST_PIZZAS } from "../../shared/list.pizza";


@Component({
  selector: 'app-details-pizza',
  templateUrl: './details-pizza.component.html',
  styleUrls: ['./details-pizza.component.scss']
})
export class DetailsPizzaComponent implements OnInit {

  pizzaToDisplay: Pizza | undefined;
  listOfPizzas: Pizza[] | undefined;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { 
  }

  ngOnInit(): void {

    this.listOfPizzas = LIST_PIZZAS;

    // @ts-ignore
    const retrievedIdFromURL: number = +this.activatedRoute.snapshot.paramMap.get('id');

    this.listOfPizzas.forEach(pizza => {
      if (pizza.id == retrievedIdFromURL) {
        this.pizzaToDisplay = pizza;
      }
    });
    console.log(this.pizzaToDisplay);
  }

  editPizza(pizzaToEdit: Pizza): void {
    const lien = ['pizza/edit', pizzaToEdit.id];
    this.router.navigate(lien);
  }
  
   delete(pizzaToDelete: Pizza): void {
    const index = LIST_PIZZAS.findIndex(pizza => pizza.id === pizzaToDelete.id);

    if (index !== -1) {
      LIST_PIZZAS.splice(index, 1);
    }
	this.router.navigate(['/pizzas']);
	alert("pizza supprime avec succes");
  }






  goBack(): void {
    this.router.navigate(['/pizzas']);
  }
}
  
 

