import { TestBed } from '@angular/core/testing';

import { ServicesPizzaService } from './services-pizza.service';

describe('ServicesPizzaService', () => {
  let service: ServicesPizzaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicesPizzaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
