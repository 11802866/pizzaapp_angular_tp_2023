import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Ajoutez cette ligne

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListPizzaComponent } from './pizza/list-pizza/list-pizza.component';
import { BorderCardDirective } from './shared/directive/border-card.directive';
import { PizzaIngredientColorPipe } from './shared/pipes/pizza-ingredient-color.pipe';
import { DetailsPizzaComponent } from './pizza/details-pizza/details-pizza.component';
import { PizzaFormComponent } from './pizza/pizza-form/pizza-form.component';
import { PizzaEditComponent } from './pizza/pizza-edit/pizza-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    ListPizzaComponent,
    BorderCardDirective,
    PizzaIngredientColorPipe,
    DetailsPizzaComponent,
    PizzaFormComponent,
    PizzaEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule // Ajoutez cette ligne
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
